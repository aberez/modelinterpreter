#include <iostream>
#include <cstring>
#include <cstdio>
//#include <ctype.h>
#include <cstdlib>
#include <typeinfo>
#pragma warning(disable : 4996)
//export MALLOC_CHECK_=0


using namespace std;


char *Err(const char *s, char c, int line, int code_line=0){
	int size=strlen(s);
	char *tmp=new char [size+35];
	sprintf(tmp,"%s %c fileline=&d .codeline=%d\n",s,c,line,code_line);
	return tmp;
}
	

//Stack def
template < class T, int max_size > class Stack
{
         T            s [max_size];
         int          top;
public:
                      Stack () { top = 0; }
         void         push ( T i );
         T            pop ();
         bool         is_empty () { return top == 0; }
         bool         is_full  () { return top == max_size; }
		 int		  get_top() { return top;}
};
 
template < class T, int max_size >
void Stack < T, max_size > :: push (T i)
{
  if ( !is_full() )
  {
    s [top++] = i;
  }
  else
    throw "Stack_is_full";
}
 
template <class T, int max_size >
T Stack < T, max_size > :: pop ()
{
  if ( !is_empty() )
    return s[--top];
  else
    throw "stack_is_empty";
}
//end Stack def




enum type_of_lex
{
	LEX_NULL, LEX_VAR, LEX_ARR, LEX_ENDVAR, LEX_Idec, LEX_Bdec,LEX_Sdec, LEX_Ddec, LEX_FUNdec, LEX_IF, LEX_THEN,  LEX_ELSE, LEX_WHILE, LEX_DO,  
			  LEX_BREAK,LEX_CONTINUE, LEX_RETURN, LEX_FOR, LEX_TO, LEX_WRITE, LEX_READ, LEX_TRUE,LEX_FALSE, 
	LEX_EOF, LEX_NOT, LEX_AND, LEX_OR, LEX_SEMI,LEX_COMMA, LEX_EQ, LEX_BR9, LEX_BR0, LEX_ASSIGN,LEX_L, LEX_G, 
			 LEX_PLUS, LEX_MINUS, LEX_MUL, LEX_SLASH, LEX_LEQ, LEX_NEQ, LEX_GEQ, LEX_BEGIN, LEX_END, LEX_SBL, LEX_SBR,
	LEX_INT, LEX_BOOL, LEX_DOUBLE, LEX_STR, LEX_FUNC,
	LEX_ID
};

  

  class Lex
{
  type_of_lex t_lex;
  double v_lex;
  char * s_lex;
public:
         Lex (type_of_lex t = LEX_NULL, double v = 0, const char * st = NULL): t_lex (t), v_lex (v) { 
					if (st) {s_lex = new char [strlen(st) + 1]; strcpy(s_lex, st); } 
					 else s_lex = NULL;
					  }
		 Lex (const Lex &a) { t_lex = a.t_lex; v_lex = a.v_lex;
					if (a.s_lex) {s_lex = new char [strlen(a.s_lex) + 1]; strcpy(s_lex, a.s_lex);} 
						else s_lex = NULL;
						}
		 Lex &       operator= (const Lex& a)
					 { 
						t_lex = a.t_lex; v_lex = a.v_lex;
						if (a.s_lex) 
						{
							if (s_lex) delete [] s_lex;
							s_lex = new char [strlen(a.s_lex) + 1 ]; 
							strcpy(s_lex, a.s_lex); 
						}
						else s_lex = NULL;
						return *this; 
					 }
         type_of_lex get_type () const { return t_lex; }
         double      get_value () const { return v_lex; }
         char * 	 get_str () const { return s_lex; }
		 ~Lex(){delete [] s_lex;} //try_
    
  friend ostream&    operator<< (ostream &s, Lex l)
                     {
                        if (l.t_lex == LEX_INT || l.t_lex == LEX_DOUBLE || l.t_lex == LEX_EOF) s << l.v_lex << ' ';
                        else s << l.s_lex << ' ';
                        return s;
                     }
                      
};//end class Lex


class Ident //elements of TID.
{
         char       * name;
		 char		* string;
         bool         declare;
         type_of_lex  type;
         bool         arr;
		 int		  arrsize;	
         int          valueint;
         double		  valuedouble;
		 int		* arrint;
		 double 	* arrdouble;

public:
                      Ident() { declare = false; arr = false; type=LEX_NULL; valueint=0;valuedouble=0;}
					 //~Ident() {if (name) delete [] name; if (string) delete [] string;
						//		if (arr) if (type == LEX_DOUBLE) delete[] arrdouble;
							//						else delete [] arrint ;}//try_
         char       * get_name () { return name; }
         void         put_name (const char *n){
								name = new char [ strlen(n)+1];
								strcpy(name,n);
                      }
         bool         get_declare () { return declare; }
         void         put_declare () { declare = true; }
         type_of_lex  get_type    () { return type; }
         void         put_type    ( type_of_lex t, int size=500 ) {
			 					  type = t; 
								  if (arr && (t == LEX_INT))
									 {arrint = new int [size]; arrsize=size; }
								  if (arr && (t == LEX_DOUBLE))
									{arrdouble = new double [size]; arrsize=size; }
							  }
         bool         get_arr  () { return arr; }
         void         put_arr  (){ arr = true; }
		 void		  put_str (char *str){
						string = new char [strlen(str)+1];
						strcpy(string,str);
					  }
		 char		*get_str(){return string;}
       
		  double get_value(int k=-43){
			if (k!=-43)
			{
			if (k >= arrsize || k<0) throw "Array Mismatch";
				else 
				if (type == LEX_INT) return arrint[k];
					else return arrdouble[k];
			}
			else
				if (type == LEX_DOUBLE) return valuedouble;
					else return valueint;

		 }	
		 void put_value(double v, int k=-43){
			if (k!=-43){
			if (k >= arrsize || k<0) throw "Array Mismatch";
				else 
				if (type == LEX_DOUBLE) arrdouble[k]=v;
					else arrint[k]=v;
			}
			else
				if (type == LEX_DOUBLE)  valuedouble=v;
					else  valueint=v;
		 }	
		 
					
			

};//end of Ident

class Tabl_ident
{
         Ident      * p;
         int          size;
		 int          top;
         
public:
		
                      Tabl_ident ( int max_size )
                      {
                        p = new Ident [ size = max_size ];
                        top = 1;
                      }
                     //~Tabl_ident () { delete [] p; }//try_
         Ident      & operator[] ( int k ) { return p[k]; }
         int          push ( const char *buf );
		 int          get_top(){return top;}
};
 
int Tabl_ident::push ( const char *buf )
{
  for ( int j = 1; j < top; j++ )
    if ( !strcmp ( buf, p[j].get_name() ) )
      return j;
  p[top].put_name(buf);
  ++top;
  return top-1;
}
//End od Tabl_ident

Tabl_ident TID ( 20), tmp(20);//MAIN TABLE of IDENTIFIERS
int		  flin=0;



class Function
{
	char *name;
	int AdrIn, AdrOut;
	type_of_lex res_type;
	
public:
	int id_num;
	Tabl_ident FTID;
	Function(char *s=NULL,int in=0,int out=0):FTID(20),AdrIn(in),AdrOut(out){  }
	//virtual ~Function(){delete []name;}//try_
	int get_in(){return AdrIn;}
	int get_out(){return AdrOut;}
	void put_in(int i){AdrIn=i;}
	void put_out(int i){AdrOut=i;}
	type_of_lex get_rest(){return res_type;}
	void put_rest(type_of_lex l){res_type=l;}
	char * get_name(){return name;}
	void   put_name (char *n){
				name = new char [ strlen(n)+1];
				strcpy(name,n);
		 }

};

class Func_tabl{
	Function      * p;
         int          size;
       
public:
		  int          top;
                      Func_tabl ( int max_size )
                      {
                        p = new Function [ size = max_size ];
                        top = 1;
                      }
					  Func_tabl(){}
                     //~Func_tabl () { delete [] p; }//try_
         Function      & operator[] ( int k ) { return p[k]; }
		  int         find (  char *buf )
						{
						  for ( int j = 1; j < top; j++ )
							if ( !strcmp ( buf, p[j].get_name() ) )
							  return j;
						  return 0;
						}
         int          push ( char *buf )
						{
						  p[top].put_name(buf);
						  ++top;
						  return top-1;
						}
};

Func_tabl Func_list(5);



class Scanner
{
         enum         state { H, IDENT, NUMB, STR, ALE, FUNC, DOUB, DELIM };
  static char       * TW    [];
  static type_of_lex  words [];
  static char       * TD    [];
  static type_of_lex  dlms  [];
         state        CS;
         FILE       * fp;
         char         c;
         char         buf [ 256 ];
         int          buf_top;
         void         clear ()
                      {
                        buf_top = 0;
                        for ( int j = 0; j < 256; j++ )
                          buf[j] = '\0';
                      }
         void         add ()
                      {
                        buf [ buf_top++ ] = c;
                      }
         int          look ( const char *buf, char **list )
                      {
                        int i = 0;
                        while (list[i])
                        {
                          if ( !strcmp(buf, list[i]) )
                            return i;
                          ++i;
                        }
                        return 0;
                      }
         void         gc ()
                      {
                        c = fgetc (fp);
						if (!c) throw "EOF before @";
                      }
public:
                      Scanner ( const char * program )
                      {
                        fp = fopen ( program, "r" );
                        flin = 1;
                        CS = H;
                        clear();
                        gc();
                      }
         Lex          get_lex ();
};
 
char *
Scanner::TW[] = {"", "var", "arr", "int", "bool", "string", "double", "func", "if", "then", "else", "while", "do", 
					 "break", "continue", "return", "for", "to", "write", "read", "true", "false", NULL
};
 
type_of_lex
Scanner::words [] = {LEX_NULL, LEX_VAR, LEX_ARR, LEX_Idec, LEX_Bdec,LEX_Sdec, LEX_Ddec, LEX_FUNdec, LEX_IF, LEX_THEN,  LEX_ELSE, LEX_WHILE, LEX_DO,  
								LEX_BREAK, LEX_CONTINUE, LEX_RETURN, LEX_FOR, LEX_TO, LEX_WRITE, LEX_READ, LEX_TRUE,LEX_FALSE, LEX_NULL
};
 
char *
Scanner::TD    [] = {"", "@", "!", "&", "|", ";",",", "==", "(", ")", "=", "<", ">", "+",
 "-", "*",  "/", "<=", "!=", ">=", "{", "}", "[", "]", NULL};

type_of_lex
Scanner::dlms  [] = {LEX_NULL, LEX_EOF, LEX_NOT, LEX_AND, LEX_OR, LEX_SEMI, LEX_COMMA, LEX_EQ, 
					 LEX_BR9, LEX_BR0, LEX_ASSIGN,
                     LEX_L, LEX_G, LEX_PLUS, LEX_MINUS, LEX_MUL, LEX_SLASH, 
                     LEX_LEQ, LEX_NEQ, LEX_GEQ, LEX_BEGIN, LEX_END, LEX_SBL, LEX_SBR,
                      LEX_NULL};
 
Lex Scanner::get_lex () 
{
  int d = 0, j, k = 1;
  double f = 0;
  int num_ID;
  CS = H;
 

  do
  {
    switch(CS)
    {
      case H:
        if ( c==' ' || c == '\n' || c== '\r' || c == '\t' ) 
        {
		  if (c == '\n') 
			  flin++;
          gc();

        }
        else if ( isalpha(c) )
        { 
          clear();
          add();
          gc();
          CS = IDENT;
        }
        else if ( c== '_' )
        {
		  clear();
          add();
          gc();
          CS = FUNC;
        }
        else if ( isdigit(c) )
        {
          d = c - '0';
          gc();
          CS = NUMB;
        }
        else if ( c== '"' )
        {
          clear();
          gc();
          CS = STR;
        }
        else if ( c== '!' || c== '<' || c== '>' || c == '=')
        { 
          clear(); 
          add(); 
          gc(); 
          CS = ALE; 
        }
        else if (c == '@')
        {
			return Lex(LEX_EOF);
        }
        else 
          CS = DELIM;
        break;
        
  
      case IDENT:
        if ( isalpha(c) || isdigit(c) ) 
        {
          add(); 
          gc();
        }
        else if ( j = look (buf, TW) )
          return Lex (words[j], j, buf);
		else
        {
          num_ID = TID.push(buf);
          return Lex (LEX_ID, num_ID, buf);
        }
        break;
       
      case FUNC:
        if ( isalpha(c) || isdigit(c) ) 
        {
          add(); 
          gc();
        }
        else
        {
          num_ID = TID.push(buf);
          return Lex (LEX_FUNC, num_ID, buf);
        }
        break;
        
        
      case NUMB:
	    if ( isdigit(c) ) 
        { 
			d = d * 10 + (c - '0'); 
			gc();
        }
        else
        if ( c == '.' )
        {
			CS = DOUB;
			gc();
		}
		else
		{
		  return Lex ( LEX_INT, d, "INT");
		}
		break;
		
      case DOUB:
	    if ( isdigit(c) ) 
        {
			f = f * 10 + (c - '0'); 
			k = k * 10;
			gc();
        }
        else
          return Lex ( LEX_DOUBLE, f / k + d, "DOB" );
		break;
      
      case STR:
        if ( c == '"' )
        {
			gc();
			return Lex (LEX_STR, 0, buf);
        }
        else if (c == '@' )
          throw "unexpected EOF '' were needed  ";
        else
        {
		  	add();
            gc();
		}
        break;
        
      case ALE:
        if ( c== '=')
        {
          add();
          gc();
          j = look ( buf, TD );
          return Lex ( dlms[j], j, buf);
        }
        else
		{
			j = look ( buf, TD );
			return Lex ( dlms[j], j, buf);
        }
        break;
        
      case DELIM:
        clear();
        add();
        if ( j = look ( buf, TD) )
        {
          gc();
          return Lex ( dlms[j], j , buf);
        }
        else
		  throw Err("unexpect char   ", c, flin);
      break;
    
    }//end switch
  } while (true);
}//end Scanner:get_lex


//!!!!!!_____POLIZ_______!!!!!!
class PolizElem
{
protected:
	char *name;
	
public:
			PolizElem(){}
			PolizElem(char *s) { name = new char [strlen(s) + 1]; strcpy(name, s); }
			char* get_name() {return name;}
			
	virtual ~PolizElem() { delete[] name;}
	virtual int Make(int cur) const = 0;


friend ostream&    operator<< (ostream &s, PolizElem *l)
                     {
						 if (l->name)
                        s << l->name; else s<<"NULL";
                        return s;
                     }
  
};

  Stack < PolizElem*, 1000 >  poliz_st;


class P_Const: public PolizElem
{	
public:
	//P_Const(const Lex &l): PolizElem(l) {}
	P_Const() {}
	virtual P_Const* Clone() const = 0;
	int Make(int cur) const { poliz_st.push(Clone()); return cur + 1;}
}; 

///Tipi Dannich
class P_Numb: public P_Const
{
public:
	virtual double get() const = 0;
	virtual int type() const = 0;
	virtual void write() const =0;
};



class P_Int : public P_Numb
{
	int n;
public:
	P_Int(int i)
		{ n = i; name = new char [20]; sprintf(name,"%d", n);}
	P_Int *Clone() const { return new P_Int(n); }
	double get() const {return n;}
	int type() const {return 1;}
	void write() const {cout<<n<<" "; return;}
};
class P_Double : public P_Numb
{
	double d;
public:	
	P_Double(double i)
		{ d = i; name = new char [20]; sprintf(name,"%f", d);}
    P_Double *Clone() const { return new P_Double(d); }
    double get() const {return d;}
    int type() const {return 0;}
	void write() const{cout<<d<<" "; return ;}
};
class P_Bool : public P_Numb
{
int n;
public:
	P_Bool(int i){
		//if (i<0 || i>1) throw "bad define num dor bool. P_bool(i)";
		n = (i!=0); name = new char [20]; sprintf(name,"bol%d", n);}
	P_Bool *Clone() const { return new P_Bool(n); }
	double get() const {return n;}
	int type() const {return 1;}
	void write()const {cout<<(bool) n<<" ";}
};

class P_String : public P_Numb
{
double hash_s( const char *str) const
    {
        long hash = 5381;
		int i=0;		
        int c;

        while (c = str[i++])
            hash = ((hash << 5) + hash) + c;

        return hash;
    }
public:
	P_String(char *s){ name = new char [strlen(s) + 1]; strcpy(name, s);}
	P_String *Clone() const { return new P_String(name);}
	//char *get_str() const {return name;}
	double get() const {return hash_s(name);} //need hash func
	int type() const {return -1; }
	void write() const {cout<<name<<" ";}
};
////END Tipi Dannich


/////logicheskie perechodi
class P_LogicGo: public PolizElem	  
{
protected:	int dest;
public:
	P_LogicGo(int i){
		name = new char[20];
		dest=i;
	}
};

class P_Go : public P_LogicGo
{
public:
	P_Go(int i): P_LogicGo(i) {
		sprintf(name, "%dGO", i);
		}
	int Make(int k) const { return dest; }
};

class P_TGo : public P_LogicGo 
{
public:
	P_TGo(int i): P_LogicGo(i) {
		sprintf(name, "%dTGO", i);
		}
	int Make(int k) const
	{
		P_Numb *t = dynamic_cast<P_Numb*>(poliz_st.pop());
		if ((int)t->get()) { delete t; return dest; }
		else { delete t; return k + 1;}
	}
};


class P_FGo : public P_LogicGo 
{
public:
	P_FGo(int i): P_LogicGo(i) {
		sprintf(name, "%dFGO", i);
		}
	int Make(int k) const
	{
		P_Numb *t = dynamic_cast<P_Numb*>(poliz_st.pop());
		if (!(int)t->get()) { delete t; return dest; }
		else { delete t; return k + 1;}
	}
};
class P_SFGo : public P_LogicGo 
{
public:
	P_SFGo(int i): P_LogicGo(i) {
		sprintf(name, "%dFGO", i);
		}
	int Make(int k) const
	{
		P_Numb *t = dynamic_cast<P_Numb*>(poliz_st.pop());
		poliz_st.push(new P_Int(t->get()));
		if (!(int)t->get()) { delete t; return dest; }
		else { delete t; return k + 1;}
	}
};

/////END logicheskie perechodi

class P_Addr : public P_Const //ukazivaet mesto v TID
{
	int TID_line;
	int cell;
public:
	P_Addr(int l, int i=-43): TID_line(l), cell(i) {name = new char [20]; sprintf(name, "line%dcell%d", TID_line, cell);}
	//P_Addr(int tl, int li) { line = li; tableline = tl; j = -1; name = new char [20]; sprintf(name, "tl%d", tl); }
	P_Addr *Clone() const {return new P_Addr(TID_line, cell);}
	int gettl() const {return TID_line;}
	int getcel() const {return cell;} 
};



class P_Putid: public PolizElem { //pit id VALUE in poliz_st PolizSig
	int TIDline;
public:
	P_Putid(int tl) {TIDline = tl; name = new char [20]; sprintf(name, "Putid%d", tl); }
	int Make(int k) const
	{
		if (TID[TIDline].get_arr()) { poliz_st.push(new P_Int (TIDline)); }
		else 
		{
		if (TID[TIDline].get_type() == LEX_INT)
		 poliz_st.push(new P_Int ( (int)TID[TIDline].get_value() )); 
		if (TID[TIDline].get_type() == LEX_BOOL)
		 poliz_st.push(new P_Int ( (bool)TID[TIDline].get_value() ));
		else if (TID[TIDline].get_type() == LEX_DOUBLE)
			poliz_st.push(new P_Double (TID[TIDline].get_value()));
		else if (TID[TIDline].get_type() == LEX_STR)
			poliz_st.push(new P_String( TID[TIDline].get_str() ));
		}
		return k + 1;
	}
};

class P_Func : public PolizElem
{ 
public:
	P_Func() {}
};

class P_RetAdr: public P_Func
{
	int fnum;
	int place;
public:
	P_RetAdr (int i, int j):fnum(i),place(j){name = new char [20]; sprintf(name, "RetAddr");}
	int Make(int k) const{
		Func_list[fnum].put_out(place);
		return k+1;
	}
};

class P_FuncIn:public P_Func { 
int fnum;
public:
	P_FuncIn(int i=0):fnum(i){name = new char [20]; sprintf(name, "FuncIn");} 
	int Make(int k) const
	{
		int elnum=Func_list[fnum].id_num;
		while (elnum>0)
		{
			P_Numb *t = dynamic_cast<P_Numb*>(poliz_st.pop()); 
			if (t->type()>=0) Func_list[fnum].FTID[elnum].put_value(t->get());
			else Func_list[fnum].FTID[elnum].put_str(t->get_name());
			--elnum;
		}
		tmp=TID;
		TID=Func_list[fnum].FTID;
		Func_list[fnum].FTID=tmp;
		return k+1;
	}
};


class P_FuncOut:public P_Func { 
int fnum;
public:
	P_FuncOut(int i=0):fnum(i){name = new char [20]; sprintf(name, "FuncOut");} 
	int Make(int k) const
	{
		tmp=TID;
		TID=Func_list[fnum].FTID;
		Func_list[fnum].FTID=tmp;
		return Func_list[fnum].get_out();
	}
};
class P_Write: public P_Func { 
public:	
	P_Write(){name = new char [20]; sprintf(name, "Write");} 

	int Make(int k) const
	{
		P_Numb *t = dynamic_cast<P_Numb*>(poliz_st.pop()); 
		t->write(); 
		delete t; 
		return k + 1;
	}
};

class P_Read: public P_Func{
public:
	P_Read(){name = new char [20]; sprintf(name, "Read");} 
	
	int Make(int k) const
	{
		P_Addr *t = dynamic_cast<P_Addr*>(poliz_st.pop()); 
		int lin = t->gettl();
		int cel = t->getcel(); 
		int tmp_int;
		bool tmp_bool;
		char *tmp_char=new char [256];
		double tmp_double;
		try{
			switch (TID[lin].get_type()){
				case LEX_INT:{
							if (!TID[lin].get_arr())
								cout<<"insert INT for "<<TID[lin].get_name()<<":";
							else
								cout<<"insert INT for "<<TID[lin].get_name()<<"["<<cel<<"]:";
							cin>>tmp_int;
							if (cin.fail()) throw "int";
							TID[lin].put_value(tmp_int,cel);
							break;
						}
				case LEX_DOUBLE:{
							if (!TID[lin].get_arr())
								cout<<"insert DOUBLE for "<<TID[lin].get_name()<<":";
							else
								cout<<"insert DOUBLE for "<<TID[lin].get_name()<<"["<<cel<<"]:";
							cin>>tmp_double;
							if (cin.fail()) throw "doub";
							TID[lin].put_value(tmp_double,cel);
							break;
						}
				case LEX_BOOL:{
							cout<<"insert BOOL (0 - true, !0 - true) for "<<TID[lin].get_name()<<":";
							cin>>tmp_int;
							tmp_int=(tmp_int!=0);
							if (cin.fail()) throw "bool";
							TID[lin].put_value(tmp_int,cel);
							break;
						}
				case LEX_STR:{
							cout<<"insert STR for "<<TID[lin].get_name()<<":";
							//cin>>tmp_char;
							if (!fscanf(stdin,"%s",tmp_char))
							throw "str";
							TID[lin].put_str(tmp_char);
							break;
						}
				default:throw "cant read uncknown type";
			}
		}
		catch (const char* er)
		  {
			  cin.getline(tmp_char,250);
			  cerr << "exception caught in cin>>" <<er << endl;
			  throw "read fail";
		  }
			
			
		delete t; 
		return k + 1;
	}
};


class P_Assign: public P_Func {//get number and cell (array or not) adress. and make assignment I:=E
	public:P_Assign() {name = new char [5]; sprintf(name, "=");} 
	int Make(int k) const 
	{
		P_Numb *t1 = dynamic_cast<P_Numb*>(poliz_st.pop());
		P_Addr *t2 = dynamic_cast<P_Addr*>(poliz_st.pop());
		int lin = t2->gettl();
		int cel = t2->getcel();
		if (t1->type()<0) TID[lin].put_str(t1->get_name()); else
			TID[lin].put_value(t1->get(),cel);
		delete t1;
		delete t2;
		return k + 1;
	}
};


class P_PutArrCel: public P_Func {//get arr_num and cell_num from stack and push array cell VALUE into stack
	public:P_PutArrCel() { name = new char [20]; sprintf(name, "arr[cell]"); } 
	int Make(int k) const
	{
		P_Int *t1 = dynamic_cast<P_Int*>(poliz_st.pop());
		P_Int *t2 = dynamic_cast<P_Int*>(poliz_st.pop());
		int cel = t1->get();
		int line = t2->get();
		
		if (TID[line].get_type() == LEX_INT)
		 poliz_st.push(new P_Int ((int)TID[line].get_value(cel))); 		  
		else 
		 poliz_st.push(new P_Double (TID[line].get_value(cel)));

		delete t1;
		delete t2;
		return k + 1;
	}
};

class P_ArrAdr: public P_Func {//a[5]:=E get arr adres and cell num. put cell address
	public:P_ArrAdr() {name = new char [10]; sprintf(name, "[adr]"); } 
	int Make(int k) const
	{
		P_Int *t1 = dynamic_cast<P_Int*>(poliz_st.pop());
		P_Addr *t2 = dynamic_cast<P_Addr*>(poliz_st.pop());
		int lin = t2->gettl();
		int cel = t1->get();
		poliz_st.push(new P_Addr (lin, cel));
		delete t1;
		delete t2;
		return k + 1;
	}
};

class P_EQ: public P_Func { 
public:
	P_EQ() {name = new char [5]; sprintf(name, "=="); } 
	int Make(int k) const {
		P_Numb *t1 = dynamic_cast<P_Numb*>(poliz_st.pop());
		P_Numb *t2 = dynamic_cast<P_Numb*>(poliz_st.pop());
		int i1 = t1->get();
		int i2 = t2->get();
		if (t1->get() == t2->get()) poliz_st.push(new P_Int(1));
		else poliz_st.push(new P_Int(0));
		delete t1;
		delete t2;
		return k + 1;
	} 
};

class P_NEQ: public P_Func { 
public:
	P_NEQ() {name = new char [5]; sprintf(name, "!="); } 
	int Make(int k) const {
		P_Numb *t1 = dynamic_cast<P_Numb*>(poliz_st.pop());
		P_Numb *t2 = dynamic_cast<P_Numb*>(poliz_st.pop());
		int i1 = t1->get();
		int i2 = t2->get();
		if (t1->get() != t2->get()) poliz_st.push(new P_Int(1));
		else poliz_st.push(new P_Int(0));
		delete t1;
		delete t2;
		return k + 1;
	} 
};



class P_L: public P_Func { 
public:
	P_L() {name = new char [5]; sprintf(name, "<"); } 
	int Make(int k) const {
		P_Numb *t1 = dynamic_cast<P_Numb*>(poliz_st.pop());
		P_Numb *t2 = dynamic_cast<P_Numb*>(poliz_st.pop());
		int i1 = t1->get();
		int i2 = t2->get();
		if (t1->get() > t2->get()) poliz_st.push(new P_Int(1));
		else poliz_st.push(new P_Int(0));
		delete t1;
		delete t2;
		return k + 1;
	} 
};
class P_G: public P_Func { 
public:
	P_G() {name = new char [5]; sprintf(name, ">"); } 
	int Make(int k) const {
		P_Numb *t1 = dynamic_cast<P_Numb*>(poliz_st.pop());
		P_Numb *t2 = dynamic_cast<P_Numb*>(poliz_st.pop());
		int i1 = t1->get();
		int i2 = t2->get();
		if (t1->get() < t2->get()) poliz_st.push(new P_Int(1));
		else poliz_st.push(new P_Int(0));
		delete t1;
		delete t2;
		return k + 1;
	} 
};


class P_LE: public P_Func { 
public:
	P_LE() {name = new char [5]; sprintf(name, "<="); } 
	int Make(int k) const {
		P_Numb *t1 = dynamic_cast<P_Numb*>(poliz_st.pop());
		P_Numb *t2 = dynamic_cast<P_Numb*>(poliz_st.pop());
		int i1 = t1->get();
		int i2 = t2->get();
		if (t1->get() >= t2->get()) poliz_st.push(new P_Int(1));
		else poliz_st.push(new P_Int(0));
		delete t1;
		delete t2;
		return k + 1;
	} 
};

class P_GE: public P_Func { 
public:
	P_GE() {name = new char [5]; sprintf(name, ">="); } 
	int Make(int k) const {
		P_Numb *t1 = dynamic_cast<P_Numb*>(poliz_st.pop());
		P_Numb *t2 = dynamic_cast<P_Numb*>(poliz_st.pop());
		int i1 = t1->get();
		int i2 = t2->get();
		if (t1->get() <= t2->get()) poliz_st.push(new P_Int(1));
		else poliz_st.push(new P_Int(0));
		delete t1;
		delete t2;
		return k + 1;
	} 
};

class P_Plus: public P_Func { 
	public:P_Plus(){ name = new char [3]; sprintf(name, "+");}
	int Make(int k) const {
		P_Numb *t1 = dynamic_cast<P_Numb*>(poliz_st.pop());
		P_Numb *t2 = dynamic_cast<P_Numb*>(poliz_st.pop());
		if (t1->type() && t2->type()) poliz_st.push(new P_Int(t1->get() + t2->get()));
		else poliz_st.push(new P_Double(t1->get() + t2->get()));
		delete t1;
		delete t2;
		return k + 1;
	}  
};

class P_Minus: public P_Func { 
	public:P_Minus(){ name = new char [3]; sprintf(name, "-");}
	int Make(int k) const {
		P_Numb *t1 = dynamic_cast<P_Numb*>(poliz_st.pop());
		P_Numb *t2 = dynamic_cast<P_Numb*>(poliz_st.pop());
		if (t1->type() && t2->type()) poliz_st.push(new P_Int(t2->get() - t1->get()));
		else poliz_st.push(new P_Double(t2->get() - t1->get()));
		delete t1;
		delete t2;
		return k + 1;
	}  
};

class P_Mul: public P_Func { 
	public:P_Mul(){ name = new char [3]; sprintf(name, "*");}
	int Make(int k) const {
		P_Numb *t1 = dynamic_cast<P_Numb*>(poliz_st.pop());
		P_Numb *t2 = dynamic_cast<P_Numb*>(poliz_st.pop());
		if (t1->type() && t2->type()) poliz_st.push(new P_Int(t2->get() * t1->get()));
		else poliz_st.push(new P_Double(t2->get() * t1->get()));
		delete t1;
		delete t2;
		return k + 1;
	}   
};

class P_Del: public P_Func { 
	public:
		P_Del(){ name = new char [3]; sprintf(name, "/");}
	   
	int Make(int k) const {
		P_Numb *t1 = dynamic_cast<P_Numb*>(poliz_st.pop());
		P_Numb *t2 = dynamic_cast<P_Numb*>(poliz_st.pop());
		if (t1->get()==0) throw "devide by 0";
		if (t1->type() && t2->type()) poliz_st.push(new P_Int((int)t2->get() / (int)t1->get()));
		else poliz_st.push(new P_Double(t2->get() / t1->get()));
		delete t1;
		delete t2;
		return k + 1;
	}
};

class P_UMinus: public P_Func { 
	public:P_UMinus(){ name = new char [3]; sprintf(name, "~");}  
	int Make(int k) const {
		P_Numb *t = dynamic_cast<P_Numb*>(poliz_st.pop());
		if (t->type()) poliz_st.push(new P_Int((int)(-t->get())));
		else poliz_st.push( new P_Double(-t->get()) ) ;
		delete t;
		return k + 1;
	}
};

class P_Not: public P_Func { 
	public:P_Not(){ name = new char [3]; sprintf(name, "!");} 
	int Make(int k) const {
		P_Numb *t = dynamic_cast<P_Numb*>(poliz_st.pop());
		poliz_st.push(new P_Int(!((int)t->get())));
		delete t;
		return k + 1;
	}
};


//END POLIZ CLASSES



class Poliz
{
         PolizElem        ** p;
         int          size;
         int          free;

public:
					 Poliz (int max_size)
                      {
						size = max_size;  
                        p = new PolizElem* [size];
                        free = 0;
                      }
                    // ~Poliz() {delete [] p;}//try_
         void        put (PolizElem *l )
                      {
                        p [free] = l;
                        free++;
                      }
         void        put ( PolizElem *l, int place) { p [place] = l; 
					}
         int         blank    () { return free++; }
         int         get_free () { return free; }
         PolizElem * operator[] (int index)
                      {
                        if ( index > size )
                          throw "POLIZ:out of array";
                        else
                          if ( index > free )
                            throw "POLIZ:indefinite element of array";
                          else
                            return p[index];
                      }
         void         print ()
                      {
				       cout << free<< endl;
				        for (int i = 0; i < free; ++i )
                           cout << i<< ":  " <<p[i]<<endl;
                                                
                      }
};


class Parser 
{
         Lex          curr_lex;
         type_of_lex  c_type;
         double       c_val;
         char*        c_str;
         int          c_line;
         Scanner      scan;
         
         Stack < int, 100 > st_int;
         Stack < type_of_lex, 100 >  st_lex;
		 Stack < int, 100> st_break;
		 Stack  < int , 100> st_continue;
		 Stack <int, 100> st_return, st_and,st_or;
		
         
         void         P();
		 void		  Opisaniya();
		 void		  Opisanie();
		 void		  Functii();
         void         Operatori();
         void         Operator();
         void         E();
         void         E1();
         void         T();
         void         K();
         void         M();
         void         F();
         
         void         dec ( type_of_lex type);
         void         check_id ();
         void         check_op ();
         void         check_num ();
         void         eq_bool ();
         void         check_id_in_read ();
         
         void         gl ()
                      { curr_lex = scan.get_lex();
		                c_type = curr_lex.get_type();
                        c_val = curr_lex.get_value();
                        c_line = flin;;
                        c_str = curr_lex.get_str();
                      }
public:
         Poliz        prog;
                      Parser (const char *program ) : scan (program),prog (500) {}
         void         analyze();
		 void		  execute();
};
 

void Parser::analyze ()
{
  gl();
  P();
}

void Parser::P ()
{	  
	prog.blank();
	if (c_type == LEX_VAR)
	{
	  gl();
	  Opisaniya();	 
	} 
	while (c_type == LEX_FUNdec){
		Functii();
	}
	prog.put(new P_Go(prog.get_free()),0);
	while (c_type != LEX_EOF){
		Operatori();
	}
}

void Parser::Opisaniya (){
	while(c_type==LEX_Idec || c_type==LEX_Ddec || c_type==LEX_Bdec || c_type==LEX_Sdec){
		Opisanie();
	}

}

void Parser::Opisanie (){
	
	bool arr=false;
    type_of_lex	id_type=LEX_NULL;
	type_of_lex last_lex=LEX_NULL;
	switch(c_type)
	{
		case LEX_Idec: {id_type=LEX_INT;break;}
		case LEX_Ddec: {id_type=LEX_DOUBLE;break;}
		case LEX_Bdec: {id_type=LEX_BOOL;break;}
		case LEX_Sdec: {id_type=LEX_STR;break;}
		default:throw "unknown type";
	}
	gl();
	if (c_type==LEX_ARR) 
	{
		arr=true; 
		if (id_type==LEX_Bdec || id_type==LEX_Sdec) throw "arr-type mismatch";
		gl();
	}
	do
	{
		if (c_type!=LEX_ID) throw "expected ID in opisaniye";
		int i=TID.push(c_str);
		if (TID[i].get_declare()) throw "double declaration";
		TID[i].put_declare();
		if (arr) TID[i].put_arr();
		TID[i].put_type(id_type);
		gl();
		if (c_type!=LEX_SEMI && c_type!=LEX_COMMA) throw "expect , or ; in var_end";
		else {last_lex=c_type; gl();}
	} while (last_lex!=LEX_SEMI);

}


void Parser::Functii(){
	gl();//first is func
	if (c_type!=LEX_FUNC) throw "expected func_ID in functii";
	if (Func_list.find(c_str)) throw "func double declaration";
	int i = Func_list.push(c_str);
	gl(); if (c_type!=LEX_BR9) throw "( expect";
	tmp=TID;
	TID=Func_list[i].FTID;
	gl(); 
	if (c_type!=LEX_BR0) Opisaniya();
	if (c_type!=LEX_BR0) throw ") expect";
	gl();
	switch(c_type)
	{
		case LEX_Idec: {Func_list[i].put_rest(LEX_INT);gl();break;}
		case LEX_Ddec: {Func_list[i].put_rest(LEX_DOUBLE);gl();break;}
		case LEX_Bdec: {Func_list[i].put_rest(LEX_BOOL);gl();break;}
		case LEX_Sdec: {Func_list[i].put_rest(LEX_STR);gl();break;}
		case LEX_BEGIN: {Func_list[i].put_rest(LEX_NULL);break;}
		default:throw "unknown type";
	}
	Func_list[i].id_num=TID.get_top()-1;
	Func_list[i].put_in(prog.get_free());
	prog.put(new P_FuncIn(i));
	Operatori();
	prog.put(new P_FuncOut(i));
	int f_out=prog.get_free()-1;
	//Func_list[i].put_out(prog.blank());
	if (!st_return.is_empty()){
		while (!st_return.is_empty())
			prog.put(new P_Go(f_out),st_return.pop());
	}

	Func_list[i].FTID=TID;
	TID=tmp;
}




void Parser::Operatori ()
{
		if (c_type == LEX_BEGIN)
		{
			gl();
			while (c_type != LEX_END) Operator();
		    gl();
		}
		else throw "no { in Operatori()";
}


void Parser::Operator ()
{
  int pl0, pl1, pl2, pl3;
  if (c_type == LEX_BEGIN){
	  Operatori();
  } 
  else if (c_type == LEX_IF)
  {
    gl();
    E();
    eq_bool(); 
  
	pl2 = prog.blank();

    if (c_type == LEX_THEN)
    {
      gl();
      Operator();

      pl3 = prog.blank();
      
      prog.put (new P_FGo(prog.get_free()), pl2);
      if (c_type == LEX_ELSE)
      {
        gl();
        Operator();
        prog.put (new P_Go(prog.get_free()), pl3);  

      }
      else
        prog.put (new P_Go(prog.get_free()), pl3);
    }
    else
      throw "exepted THEN after IF";
  }//end if then else
  
  else if (c_type == LEX_WHILE)
  {
	int break_t=st_break.get_top();
    pl0 = prog.get_free();
  
	gl();
    E();
    eq_bool();

    pl1 = prog.blank();
  
	if (c_type == LEX_DO)
    {
      gl();
      Operator();
    
      prog.put (new P_Go(pl0));
      prog.put (new P_FGo(prog.get_free()), pl1);
	  while (st_break.get_top()>break_t)
		  prog.put (new P_Go(prog.get_free()), st_break.pop());
  
	}
    else
      throw "exepted DO";
  }//end while do
   else if (c_type == LEX_FOR)
  {
	  int break_t=st_break.get_top();
	  int cont_t=st_continue.get_top();
	  
  
	gl();
    Operator();//initialization_expression

	int pl_cond = prog.get_free();
	E();///loop_condition
    eq_bool();

	int pl_false = prog.blank();
	int pl_true = prog.blank();
	
	int pl_inc = prog.get_free();
	gl();
	Operator(); //increment_expression
	prog.put(new P_Go(pl_cond));

	prog.put(new P_Go(prog.get_free()),pl_true);
	
	if (c_type == LEX_DO)
    {
      gl();
      Operator();
    
      prog.put (new P_Go(pl_inc));
	  while (st_continue.get_top()>cont_t)
		  prog.put (new P_Go(pl_inc), st_continue.pop());
      prog.put (new P_FGo(prog.get_free()), pl_false);
	  while (st_break.get_top()>break_t)
		  prog.put (new P_Go(prog.get_free()), st_break.pop());
  
	}
    else
      throw "exepted DO";
  }//end for
  else if (c_type==LEX_BREAK){
	  st_break.push(prog.blank());
	  gl();
	  if (c_type !=LEX_SEMI) throw "; expected";
	  gl();
  }
  else if (c_type==LEX_CONTINUE){
	  st_continue.push(prog.blank());
	  gl();
	  if (c_type !=LEX_SEMI) throw "; expected";
	  gl();
  }
   else if (c_type==LEX_RETURN){
	   gl();
	   E();
	  st_return.push(prog.blank());
	  st_lex.pop();
	  if (c_type !=LEX_SEMI) throw "; expected";
	  gl();
  }
  else if (c_type == LEX_WRITE)
  {
	do{
	gl();
	E();//proverka ne nuzhna, t.k. E() vsegda chto-to
	st_lex.pop();
	prog.put(new P_Write()); 
	} while (c_type == LEX_COMMA);
	if (c_type != LEX_SEMI) throw "exepted ;";
	gl();  
  }//end write
  else if (c_type == LEX_READ)
  {
	gl();
	if ( c_type != LEX_ID ) throw "expected id in read";
	
	prog.put (new P_Addr(TID.push(c_str)));	
    int i = TID.push(c_str);
	st_lex.push(TID[i].get_type());
    gl();
    if (c_type == LEX_SBL)
    {
		gl();
		E();
		eq_bool();
		
		if (c_type != LEX_SBR) throw "exepted ]";
		else gl();
		
		if ((TID[i].get_declare()) && !(TID[i].get_arr()))
		throw "NOT array []";
		
		TID[i].put_arr();
		prog.put(new P_ArrAdr());
	}
	prog.put(new P_Read());
  
	if (c_type != LEX_SEMI) throw "exepted ;";
	gl(); 
  }

  
  else if ( c_type == LEX_ID )//proverit' st_lex.push()
  {
	prog.put (new P_Addr(TID.push(c_str)));
	
    int i = TID.push(c_str);
	st_lex.push(TID[i].get_type());
    gl();
    if (c_type == LEX_SBL)
    {
		gl();
		E();
		eq_bool();
		
		if (c_type != LEX_SBR) throw "exepted ]";
		else gl();
		
		if ((TID[i].get_declare()) && !(TID[i].get_arr()))
		throw "NOT array []";
		
		TID[i].put_arr();
		prog.put(new P_ArrAdr());
	}
	
	if ( c_type == LEX_ASSIGN )//NUZHNO POSMOTRET'  st_lex.pop(); GDE TAM LEX_ID
    {
		st_lex.push(LEX_ASSIGN);
      gl();
      E();
	  check_op();   //!!

	  prog.put (new P_Assign());
    }
    else
      throw "exepted = after id";
    
    if (c_type != LEX_SEMI) throw "exepted ;";
		else gl();  
  }//end =
  else
	if (c_type == LEX_FUNC){
		int fun_num=Func_list.find(c_str);
		gl();
		if (c_type!=LEX_BR9) throw "( expect";
		int i=0;
		gl();
		while (c_type!=LEX_BR0){
			++i;
			if (i>Func_list[fun_num].id_num) throw "too many arguments";
			K();
			type_of_lex ct=st_lex.pop();
			if (ct!=Func_list[fun_num].FTID[i].get_type()) throw "function type mismatch";
			if (c_type!=LEX_COMMA && c_type!=LEX_BR0){cerr<<"unexpected lex"; throw c_str;}
			if (c_type==LEX_COMMA) gl();
		}
		gl();

		prog.put(new P_RetAdr(fun_num,prog.get_free()+2));
		prog.put(new P_Go(Func_list[fun_num].get_in()));
		//prog.put(new P_Go(prog.get_free()),Func_list[fun_num].get_out());
		if (c_type != LEX_SEMI) throw "exepted ;";
			else gl();  
	}
	else
    if (c_type==LEX_SEMI) gl();
		else throw "unknown operator";	
}





void Parser::E () 
{

    E1();
   
 	while ( c_type == LEX_OR )
	{
		int pl=prog.blank();
		prog.put(new P_Int(1));
		st_or.push(prog.blank());
		prog.put(new P_FGo(prog.get_free()),pl);
		st_lex.push (c_type); 
		gl(); 
		E1();
		check_op();
		//prog.put(new P_Or());
	}
	while (!st_or.is_empty()) prog.put(new P_Go(prog.get_free()),st_or.pop());
}



void Parser::E1 ()
{

	T();
	while ( c_type == LEX_AND )
	{
		st_and.push(prog.blank());
		st_lex.push (c_type); 
		gl(); 
		T();
		check_op();
		//prog.put(new P_And());
	}
	while (!st_and.is_empty()) prog.put(new P_SFGo(prog.get_free()),st_and.pop());
}

 
void Parser::T ()
{	

  K();

  while ( c_type == LEX_EQ || c_type == LEX_NEQ || c_type == LEX_L || c_type == LEX_G ||
       c_type == LEX_LEQ || c_type == LEX_GEQ )
  {
    st_lex.push (c_type);
    type_of_lex ct = c_type;
    gl();
    K();
    check_op();
    
    switch (ct){
		case LEX_EQ: { prog.put (new P_EQ()); break; }
		case LEX_NEQ: { prog.put (new P_NEQ()); break; }
		case LEX_L: { prog.put (new P_L()); break; }
		case LEX_G: { prog.put (new P_G()); break; }
		case LEX_LEQ: { prog.put (new P_LE()); break; }
		case LEX_GEQ: { prog.put (new P_GE()); break; }
	}
  }
}

void Parser::K()
{

 	M();
  
	while ( c_type == LEX_PLUS || c_type == LEX_MINUS )
	{
		st_lex.push (c_type);
		type_of_lex ct = c_type;
		gl();
		M();
		check_op();
	    switch (ct)	{
			case LEX_PLUS: { prog.put (new P_Plus()); break; }
			case LEX_MINUS: { prog.put (new P_Minus()); break; }
		}
	}

  
}

void Parser::M ()
{
  F();
 
  while ( c_type == LEX_MUL || c_type == LEX_SLASH)
  {
    st_lex.push (c_type);
    type_of_lex ct = c_type;
    gl();
    F();
    check_op(); 
    switch (ct){
		case LEX_MUL: { prog.put (new P_Mul()); break; }
		case LEX_SLASH: { prog.put (new P_Del()); break; }
	}
  }
}



 
void Parser::F () 
{

  if ( c_type == LEX_ID ) 
  {
		int i = TID.push(c_str);
        prog.put (new P_Putid(TID.push(c_str)));
  
		
  	    if (!TID[i].get_declare()) 
			throw 	"undeclared var";
		
		gl();
		if (c_type == LEX_SBL)
		{
			gl();
			E();
			eq_bool();
			if (c_type != LEX_SBR) throw "exepted ]";
			else gl();
		    if (!TID[i].get_arr()) throw 	"not array has []";
		    prog.put (new P_PutArrCel());
		}
		else if (TID[i].get_arr()) throw 	"array hasn't []";
		
		st_lex.push(TID[i].get_type());//!
  }
  else if (c_type == LEX_FUNC){
		int fun_num=Func_list.find(c_str);
		gl();
		if (c_type!=LEX_BR9) throw "( expect";
		int i=0;
		gl();
		while (c_type!=LEX_BR0){
			++i;
			if (i>Func_list[fun_num].id_num) throw "too many arguments";
			K();
			type_of_lex ct=st_lex.pop();
			if (ct!=Func_list[fun_num].FTID[i].get_type()) throw "function type mismatch";
			if (c_type!=LEX_COMMA && c_type!=LEX_BR0){cerr<<"unexpected lex"; throw c_str;}
			if (c_type==LEX_COMMA) gl();
		}
		st_lex.push(Func_list[fun_num].get_rest());
		prog.put(new P_RetAdr(fun_num,prog.get_free()+2));
		prog.put(new P_Go(Func_list[fun_num].get_in()));
		//prog.put(new P_Go(Func_list[fun_num].get_in()));
		//prog.put(new P_Go(prog.get_free()),Func_list[fun_num].get_out());
		gl();
  }
  else if ( c_type == LEX_INT ) 
  {
    st_lex.push (LEX_INT);
    prog.put (new P_Int(c_val));
    gl();
  }
  else if ( c_type == LEX_TRUE )
  {
    st_lex.push (LEX_BOOL);
    prog.put (new P_Int(1));
    gl();
  }
  else if ( c_type == LEX_FALSE )
  {
    st_lex.push (LEX_BOOL);
    prog.put (new P_Int(0));
    gl();
  }
  else if ( c_type == LEX_DOUBLE ) 
  {
    st_lex.push (LEX_DOUBLE);
    prog.put (new P_Double(c_val));
    gl();
  }
  else if ( c_type == LEX_STR ) 
  {
    st_lex.push (LEX_STR);
    prog.put (new P_String(c_str));
    gl();
  }
  else if (c_type == LEX_NOT) 
  {
    gl(); 
    F(); 
    check_num();
    prog.put (new P_Not());
  }
  else if (c_type == LEX_MINUS) 
  {
    gl(); 
    F(); 
    check_num();
    prog.put (new P_UMinus());
  }
  else if ( c_type == LEX_BR9 ) 
  {
    gl(); 
    E();
    if ( c_type == LEX_BR0)
      gl();
    else 
      throw "exepted )";
  }
  else 
     throw "is it right statement?";  
}



void Parser::check_op () 
{
  type_of_lex t1, t2, op, r;
 
  t2 = st_lex.pop();
  op = st_lex.pop();
  t1 = st_lex.pop();
  if (t1 == LEX_STR || t2 == LEX_STR){
	  if (!(op==LEX_ASSIGN || op==LEX_EQ || op==LEX_NEQ))
		throw "bad operation for str"; 
	  if (t1!=t2) throw "str-str type mismatch";
	  r=LEX_BOOL;
	  st_lex.push(r);
	  return;
  }
  
  if (op == LEX_PLUS || op == LEX_MINUS || op == LEX_MUL || op == LEX_SLASH )
  {
	if (t1 == LEX_BOOL || t2 == LEX_BOOL) throw "find not boolean operation +-*/";
    if ((t1 == LEX_DOUBLE) || (t2 == LEX_DOUBLE))
		r = LEX_DOUBLE;
	else
		r = LEX_INT;
  }
  if (op==LEX_ASSIGN) return; //proverka?
		
  if (op == LEX_OR || op == LEX_AND || op==LEX_NOT)
	if ((t1 == LEX_DOUBLE) || (t2 == LEX_DOUBLE))
		throw "wrong types are in operation | & !";
	else	
		r = LEX_BOOL;
		
  if (op == LEX_EQ || op == LEX_L || op == LEX_G ||
       op == LEX_LEQ || op == LEX_GEQ || op == LEX_NEQ)
	r = LEX_BOOL;
		
  st_lex.push(r);
}
 
void Parser::check_num () 
{
  type_of_lex le=st_lex.pop();
  if (le!= LEX_INT && le != LEX_BOOL )
    throw "check_num: wrong type.not integer";
  else 
  {
    st_lex.push (LEX_INT);
  }
}
 
 
void Parser::eq_bool () 
{
	type_of_lex le=st_lex.pop();
  if (le!= LEX_INT && le != LEX_BOOL )
    throw "eq_bool:expression is not whole number";
}
 






void Parser::execute ()
{
	int cur = 0;
	while (cur < prog.get_free())
	{
		cur = prog[cur]->Make(cur);
	}
} 



void do_program(){
		Parser par("prog.txt");
		par.analyze();
		par.prog.print();
		cout<<endl;
		par.execute();
		cout<<endl;
		return;
}



void print_lex_analizer();

int main(){
	try{
		do_program();
	}
	catch (char c)
	{
		cout << "unexpected symbol " << c << endl;
		return 1;
	}
	catch ( const char *source )
	{
		cout << source << endl;
		return 1;
	}
	catch (...){
		cout<<"unknown error";
	}
	return 0;
}

void print_lex_analizer(){
	Scanner scan("prog.txt");
	Lex curr;
	do{
	curr=scan.get_lex();
	cout<<curr;
	}
	while (curr.get_type()!=LEX_EOF);
}

